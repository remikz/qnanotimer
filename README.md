Overview
========
QNanoTimer is a Qt C++ timer with nanosecond resolution. The standard QTimer only has millisecond resolution.

Usage
=====

	#include <QApplication>
	#include <QNanoTimer.h>
	#include <SomeListener.h>

	int main(int argc, char *argv[])
	{
		QApplication a(argc, argv);

		QNanoTimer t;
		t.setTimerType(Qt::PreciseTimer);
		t.setNanoInterval(100);

        SomeListener l;
        QObject::connect(& t, SIGNAL(timeout()), & l, SLOT(ontimeout()));

        t.start();

		return a.exec();
	}

Example
=======

	$ cd example && make example run clean
	QDateTime(2016-02-21 21:04:18.002 EST Qt::TimeSpec(LocalTime)) Elapsed=500085076 mCounter=1
	First timeout. isActive=true
	Restarting timer without single shot.
	QDateTime(2016-02-21 21:04:19.003 EST Qt::TimeSpec(LocalTime)) Elapsed=500106223 mCounter=3
	Stopping timer.
	Restarting timer with 100ms interval.
	QDateTime(2016-02-21 21:04:19.703 EST Qt::TimeSpec(LocalTime)) Elapsed=100115762 mCounter=6
	Stopping timer.
	Restarting timer with 100us interval.
	QDateTime(2016-02-21 21:04:19.804 EST Qt::TimeSpec(LocalTime)) Elapsed=155952 mCounter=9
	Stopping timer.
	Restarting timer with 100ns interval.
	QDateTime(2016-02-21 21:04:19.804 EST Qt::TimeSpec(LocalTime)) Elapsed=55679 mCounter=12
	Stopping timer.
	Restarting timer with 1ns interval PreciseType.
	QDateTime(2016-02-21 21:04:19.804 EST Qt::TimeSpec(LocalTime)) Elapsed=37 mCounter=15
	Stopping timer.
	Quitting app.

Your mileage may vary. My dev host's timer never gets more accurate than 55 microseconds in CoarseType mode, and 37 nanoseconds using a busy loop in PreciseType mode. The same performance is observed in a vanilla C program:

	$ cd example && make vanilla && ./build/vanilla
	sleep elapsed nanos:
	58146
	56934
	55894
	loop elapsed nanos:
	40
	38
	38

My host info:

	$ grep GHz /proc/cpuinfo | uniq
	model name      : Intel(R) Core(TM) i7 CPU         970  @ 3.20GHz
	$ uname -a
	Linux mintdt 3.16.0-38-generic #52~14.04.1-Ubuntu SMP x86_64 GNU/Linux

License
=======
Copyright (C) 2016 Remik Ziemlinski LGPLv3

