#ifndef QNANOTIMER_H
#define QNANOTIMER_H

#include <QObject>
#include <QThread>
#include <time.h>

class QNanoTimer : public QObject
{
    Q_OBJECT
public:
    explicit QNanoTimer(QObject *parent = 0);
    ~QNanoTimer();

    int elapsedNanos() const;
    int interval() const;
    bool isActive() const;
    bool isSingleShot() const;
    int remainingTime() const;
    void setElapsedEnabled(bool enabled);
    void setInterval(int millis);
    void setMicroInterval(int micros);
    void setMilliInterval(int millis);
    void setNanoInterval(int nanos);
    void setSingleShot(bool singleShot);
    void setTimerType(Qt::TimerType atype);
    int timerId() const;
    Qt::TimerType timerType() const;

public slots:
    void run();
    void start(int nanos);
    void start();
    void stop();

signals:
    void timeout();

protected:
    void runSingleShot();
    void runLoop();
    void sleep();
    void sleepCoarse();
    void sleepPrecise();

private:
    int mElapsedNanos;
    bool mElapsedEnabled;
    struct timespec mInterval;
    bool mIsActive;
    bool mIsSingleShot;
    struct timespec mStart;
    QThread mThread;
    Qt::TimerType mTimerType;
};


#endif // QNANOTIMER_H

//    LICENSE BEGIN
//
//    QNanoTimer - A Qt C++ Timer with nanosecond resolution.
//    Copyright (C) 2016  Remik Ziemlinski
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    LICENSE END
