#include "QNanoTimer.h"

QNanoTimer::QNanoTimer(QObject *parent) :
    QObject(parent),
    mElapsedNanos(0), mElapsedEnabled(false),
    mIsActive(false), mIsSingleShot(false),
    mTimerType(Qt::CoarseTimer)
{
    mInterval.tv_sec = mInterval.tv_nsec = 0;
}

QNanoTimer::~QNanoTimer()
{
    mThread.quit();
    mThread.wait();
}

int QNanoTimer::elapsedNanos() const
{
    return mElapsedNanos;
}

int QNanoTimer::interval() const
{
    return mInterval.tv_nsec;
}

bool QNanoTimer::isActive() const
{
    return mIsActive;
}

bool QNanoTimer::isSingleShot() const
{
    return mIsSingleShot;
}

int QNanoTimer::remainingTime() const
{
    if (!mIsActive) {
        return -1;
    }

    if (mInterval.tv_nsec <= 0) {
        return 0;
    }

    struct timespec now;
    clock_gettime(CLOCK_REALTIME, & now);

    double delta =
            (now.tv_sec  - mStart.tv_sec) * 1e9 +
            (now.tv_nsec - mStart.tv_nsec);

    return delta;
}

void QNanoTimer::run()
{
    mIsActive = true;
    if (mIsSingleShot) {
        runSingleShot();
    } else {
        runLoop();
    }
}

void QNanoTimer::runSingleShot()
{
    sleep();
    mThread.quit();
    emit timeout();
}

void QNanoTimer::runLoop()
{
    while(mIsActive) {
        sleep();
        emit timeout();
    }
}

void QNanoTimer::setElapsedEnabled(bool enabled)
{
    mElapsedEnabled = enabled;
}

void QNanoTimer::setInterval(int millis)
{
    mInterval.tv_nsec = millis * 1e6;
}

void QNanoTimer::setMicroInterval(int micros)
{
    mInterval.tv_nsec = micros * 1e3;
}

void QNanoTimer::setMilliInterval(int millis)
{
    setInterval(millis);
}

void QNanoTimer::setNanoInterval(int nanos)
{
    mInterval.tv_nsec = nanos;
}

void QNanoTimer::setSingleShot(bool singleShot)
{
    mIsSingleShot = singleShot;
}

void QNanoTimer::setTimerType(Qt::TimerType atype)
{
    mTimerType = atype;
}

void QNanoTimer::sleep()
{
    switch(mTimerType) {
    case Qt::CoarseTimer:
        sleepCoarse();
        break;
    default:
        sleepPrecise();
        break;
    }
}

void QNanoTimer::sleepCoarse()
{
    #define SLEEP() nanosleep(const_cast<const timespec*>(& mInterval), 0);
    if (mElapsedEnabled) {
        clock_gettime(CLOCK_REALTIME, & mStart);
        SLEEP();
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, & now);

        mElapsedNanos =
                (now.tv_sec  - mStart.tv_sec) * 1e9 +
                (now.tv_nsec - mStart.tv_nsec);
    } else {
        SLEEP();
    }
    #undef SLEEP
}

void QNanoTimer::sleepPrecise()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, & mStart);

    do {
        clock_gettime(CLOCK_REALTIME, & now);
    } while ((now.tv_nsec - mStart.tv_nsec) < mInterval.tv_nsec);

    mElapsedNanos =
            (now.tv_sec  - mStart.tv_sec) * 1e9 +
            (now.tv_nsec - mStart.tv_nsec);
}

void QNanoTimer::start(int nanos)
{
    mInterval.tv_nsec = nanos;
    start();
}

void QNanoTimer::start()
{
    mIsActive = true;
    moveToThread(& mThread);
    connect(& mThread, SIGNAL(started()), this, SLOT(run()));
    mThread.start();
}

void QNanoTimer::stop()
{
    disconnect(& mThread, SIGNAL(started()), this, SLOT(run()));
    mIsActive = false;
    mThread.quit();
}

int QNanoTimer::timerId() const
{
    long long id = reinterpret_cast<long long>(this);
    return (int) id;
}

Qt::TimerType QNanoTimer::timerType() const
{
    return mTimerType;
}

//    LICENSE BEGIN
//
//    QNanoTimer - A Qt C++ Timer with nanosecond resolution.
//    Copyright (C) 2016  Remik Ziemlinski
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    LICENSE END
