QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = example

INCLUDEPATH += ..

SOURCES += main.cpp ../QNanoTimer.cpp

HEADERS  += ../QNanoTimer.h listener.h

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3

