#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[])
{
    struct timespec interval, start, stop;
    const int N = 3;
    int elapsed[2*N];
    int i;

    interval.tv_sec = 0;
    interval.tv_nsec = 1;

    for(i = 0; i < N; ++i) {
        clock_gettime(CLOCK_REALTIME, & start);
        nanosleep(& interval, 0);
        clock_gettime(CLOCK_REALTIME, & stop);
        elapsed[i] = stop.tv_nsec - start.tv_nsec;
    }

    for(i = 0; i < N; ++i) {
        clock_gettime(CLOCK_REALTIME, & start);
        do { } while (0);
        clock_gettime(CLOCK_REALTIME, & stop);
        elapsed[i+N] = stop.tv_nsec - start.tv_nsec;
    }

    printf("sleep elapsed nanos:\n");
    for(i = 0; i < N; ++i) {
        printf("%d\n", elapsed[i]);
    }

    printf("loop elapsed nanos:\n");
    for(i = 0; i < N; ++i) {
        printf("%d\n", elapsed[i+N]);
    }

    return 0;
}
