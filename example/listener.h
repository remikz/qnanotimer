#ifndef LISTENER_H
#define LISTENER_H

#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QNanoTimer.h>
#include <QObject>

class Listener : public QObject
{
    Q_OBJECT
public:
    explicit Listener(QNanoTimer *timer, QObject *parent = 0) :
        QObject(parent), mCounter(0), mTimer(timer) {}

public slots:
    void timedout() {
        #define ECHO() { \
            QDateTime now(QDateTime::currentDateTime()); \
            qDebug().nospace() << now << " Elapsed=" << mTimer->elapsedNanos() \
                               << " mCounter=" << mCounter; \
        }
        ++mCounter;

        switch(mCounter) {
        case 1:
            ECHO();
            qDebug().nospace() << "First timeout. isActive="
                               << mTimer->isActive();
            mTimer->setSingleShot(false);
            qDebug() << "Restarting timer without single shot.";
            mTimer->start();
            break;
        case 3:
            ECHO();
            qDebug() << "Stopping timer.";
            mTimer->stop();
            qDebug() << "Restarting timer with 100ms interval.";
            mTimer->setMilliInterval(100);
            mTimer->start();
            break;
        case 6:
            ECHO();
            qDebug() << "Stopping timer.";
            mTimer->stop();
            qDebug() << "Restarting timer with 100us interval.";
            mTimer->setMicroInterval(100);
            mTimer->start();
            break;
        case 9:
            ECHO();
            qDebug() << "Stopping timer.";
            mTimer->stop();
            qDebug() << "Restarting timer with 100ns interval.";
            mTimer->setNanoInterval(100);
            mTimer->start();
            break;
        case 12:
            ECHO();
            qDebug() << "Stopping timer.";
            mTimer->stop();
            qDebug() << "Restarting timer with 1ns interval PreciseType.";
            mTimer->setNanoInterval(1);
            mTimer->setTimerType(Qt::PreciseTimer);
            mTimer->start();
            break;
        case 15:
            ECHO();
            qDebug() << "Stopping timer.";
            mTimer->stop();
            qDebug() << "Quitting app.";
            QApplication::quit();
            break;
        default:
            break;
        }

        #undef ECHO
    }

private:
    int mCounter;
    QNanoTimer *mTimer;
};

#endif

//    LICENSE BEGIN
//
//    QNanoTimer - A Qt C++ Timer with nanosecond resolution.
//    Copyright (C) 2016  Remik Ziemlinski
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    LICENSE END
