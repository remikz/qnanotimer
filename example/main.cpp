#include <QApplication>
#include "listener.h"
#include "QNanoTimer.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QNanoTimer t;
    t.setNanoInterval(500 * 1e6); // Half a second.
    t.setElapsedEnabled(true); // Track the actual interval.
    t.setSingleShot(true);

    Listener l(& t);
    QObject::connect(& t, SIGNAL(timeout()), & l, SLOT(timedout()));

    t.start();

    return a.exec();
}

//    LICENSE BEGIN
//
//    QNanoTimer - A Qt C++ Timer with nanosecond resolution.
//    Copyright (C) 2016  Remik Ziemlinski
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    LICENSE END
